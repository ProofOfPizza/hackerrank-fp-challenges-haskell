import Control.Monad

perimeterOfPolygon :: (Eq a, Floating a) => [(a,a)] -> a
perimeterOfPolygon points
  | points == [] = 0.0
  | otherwise = let shiftedPoints = [points] >>= (\x -> tail x ++ [head x])
                    distances = zipWith (\(x1,y1) (x2,y2) -> (sqrt (((x1-x2)^2) + ((y1-y2)^2)))) points shiftedPoints
                in sum $ distances

main :: IO ()
main = do
    _ <- getLine
    inputdata <- getContents
    let input = [(read (words str !! 0) :: Float, read (words str !! 1) :: Float ) | str <- lines inputdata]
    print $ perimeterOfPolygon input
