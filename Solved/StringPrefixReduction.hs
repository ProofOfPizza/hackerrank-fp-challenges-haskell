cutPrefix :: String -> String -> [String]
cutPrefix x y = [prefix , x `minus` prefix, y `minus` prefix] where
  prefix = map fst $ takeWhile (\(a,b) -> a==b)$ zip x y
  minus zs p = foldr (\_ (x:xs) -> xs  ) zs $ [1..(length p)]

main :: IO ()
main = do
  x <- getLine
  y <- getLine
  mapM_ (\x -> putStrLn (show (length x) ++ " " ++ x)) $ cutPrefix x y
