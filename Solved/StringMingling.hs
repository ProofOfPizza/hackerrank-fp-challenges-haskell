
main :: IO ()
main = do
  p <- getLine
  q <- getLine

  putStrLn $ concat $ zipWith (\x y -> [x,y]) p q
