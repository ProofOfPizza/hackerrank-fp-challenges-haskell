fib :: Int -> Int
fib n =  fiblist [1,0] where
  fiblist (x:y:zs) = if length (x:y:zs) < n
    then fiblist (y+x:x:y:zs)
    else x

-- This part is related to the Input/Output and can be used as it is
-- Do not modify it
main = do
    input <- getLine
    print . fib . (read :: String -> Int) $ input
