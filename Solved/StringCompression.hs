-- https://www.hackerrank.com/challenges/string-compression/problem

crunch :: String -> String
crunch xs = concat $ concat $ map g $ foldr f [(last ls)] (init ls) where
  ls = (zip xs $ repeat 1)

f = (\x acc -> if x `isEqualValue` (head acc) then [(addOne (head acc))] ++ (tail acc) else [x] ++ acc)
g = (\(a,i) -> if i>1 then [[a], show i] else [[a]])

addOne :: (Char, Int) -> (Char, Int)
addOne (ch, index) = (ch, index+1)

isEqualValue :: (Char, Int) -> (Char, Int) -> Bool
isEqualValue (a, i) (b, j) = a==b

main :: IO ()
main = do
    a <- getLine
    putStrLn $ crunch a
