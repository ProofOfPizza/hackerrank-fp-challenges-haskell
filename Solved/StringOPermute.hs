
import Control.Monad

splitEvery :: Int -> String -> [String]
splitEvery n [] = [[]]
splitEvery n str = [(fst $ splitAt n str)] ++ splitEvery n (snd $ splitAt n str)

splitAndflip :: Int -> String -> String
splitAndflip n str = concat $ map reverse $ splitEvery n str

main :: IO ()
main = do
    n_temp <- getLine
    let n = read n_temp :: Int
    rawInput <- getMultipleLines n
    mapM_ putStrLn $ map (splitAndflip 2) rawInput


getMultipleLines :: Int -> IO [String]
getMultipleLines n
    | n <= 0 = return []
    | otherwise = do
        x <- getLine
        xs <- getMultipleLines (n-1)
        let ret = (x:xs)
        return ret
