f :: [Int] -> [Int]
f   lst = (zip [0..] lst) >>= (\(a,b) -> if a `mod` 2 /= 0 then [b] else [] )



-- This part deals with the Input and Output and can be used as it is. Do not modify it.
main = do
   inputdata <- getContents
   mapM_ (putStrLn. show). f. map read. lines $ inputdata
