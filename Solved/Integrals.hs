import Text.Printf (printf)

-- This function should return a list [area, volume].

solve :: Int -> Int -> [Int] -> [Int] -> [Double]
solve l r a b =
  let dx = 0.001
      xVals = [fromIntegral l, fromIntegral l+dx..fromIntegral r]
      f x = sum $ (\(a,b) -> fromIntegral a*(x** fromIntegral b)) <$> a `zip` b
      yVals = map f xVals
      area = sum $ (\(y) -> dx*y) <$> yVals
      volume = sum $ (\(y) -> dx*pi*(y**2)) <$> yVals
  in [area, volume]


--Input/Output.
main :: IO ()
main = getContents >>= mapM_ (printf "%.1f\n"). (\[a, b, [l, r]] -> solve l r a b). map (map read. words). lines
