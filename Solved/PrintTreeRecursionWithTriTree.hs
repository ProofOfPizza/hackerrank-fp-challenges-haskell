-- https://www.hackerrank.com/challenges/fractal-trees/problem

import Data.List

data TriTree a = ET | Node a (TriTree a) (TriTree a) (TriTree a) deriving (Show, Read)

instance (Eq a) => Eq (TriTree a) where
  (==) (Node m (Node n _ _ _) (Node o _ _ _) (Node p _ _ _)) (Node m' (Node n' _ _ _) (Node o' _ _ _) (Node p' _ _ _)) =
    m == m' && n == n' && o == o' && p == p'
  (==) (Node m n p o ) (Node m' n' p' o') =
    m == m' && n == n' && o == o' && p == p'
  (==) ET ET = True
  (==) _ _ = False

singleton :: (Eq a) => a -> TriTree a
singleton x = Node x ET ET ET

lefty :: (Eq a) => a -> a -> TriTree a
lefty x y = Node x (singleton y) ET ET

righty :: (Eq a) => a -> a -> TriTree a
righty x y = Node x ET ET (singleton y)

straight :: TriTree Int -> TriTree Int
straight ET = ET
straight (Node x ET ET ET) = Node x ET (singleton x) ET
straight (Node x ET middle ET) = Node x ET (straight middle) ET
straight (Node x ET ET right) =  Node x ET ET (straight right)
straight (Node x left ET ET) = Node x (straight left) ET ET
straight (Node x left ET right) = Node x (straight left) ET (straight right)

branch :: TriTree Int -> TriTree Int
branch ET = ET
branch (Node x ET ET ET) = Node x (singleton (x-1)) ET (singleton (x+1))
branch (Node x ET middle ET) = Node x ET (branch middle) ET
branch (Node x ET ET right) = Node x ET ET (branch right)
branch (Node x left ET ET) = Node x (branch left) ET ET
branch (Node x left ET right) = Node x (branch left) ET (branch right)

widen :: TriTree Int -> TriTree Int
widen ET = ET
widen (Node x ET middle ET) = Node x ET (widen middle) ET
widen (Node x ET ET (Node y ET ET ET)) = Node x ET ET (righty y (y+1))
widen (Node x ET ET right) = Node x ET ET (widen right)
widen (Node x (Node y ET ET ET) ET ET) = Node x (lefty y (y-1)) ET ET
widen (Node x left ET ET) = Node x (widen left) ET ET
widen (Node x (Node y ET ET ET) ET (Node z ET ET ET)) = Node x (lefty y (y-1)) ET (righty z (z+1))
widen (Node x left ET right) = Node x (widen left) ET (widen right)

levelValues :: TriTree Int -> [[Int]]
levelValues ET = []
levelValues (Node x l m r) = [[x]] ++ merge (levelValues l) (levelValues m) (levelValues r)

merge :: [[Int]]-> [[Int]] -> [[Int]] -> [[Int]]
merge [] [] [] = []
merge ls [] [] = ls
merge [] ms [] = ms
merge [] [] rs = rs
merge (l:ls) [] (r:rs) = (l ++ r) : merge ls [] rs
merge (l:ls) (m:ms) (r:rs) = (l ++ m ++ r) : merge ls ms rs

straights m = [ [1..(2^(m-2))] | m <- [6,5..2] ] !!(m-1)
branches m = [1]
widenings m = [ [1..(2^(m-2) - 1)] | m <- [6,5..2] ] !!(m-1)

modifyTree :: [Int] -> (TriTree Int -> TriTree Int) -> TriTree Int -> TriTree Int
modifyTree numOfOperations operation start = foldr (\x acc -> operation acc) start numOfOperations

iteration :: Int -> TriTree Int -> TriTree Int
iteration i start =
  modifyTree (widenings i) widen $ modifyTree (branches i) branch $ modifyTree (straights i) straight start

line :: [Int] -> String
line ones = concat $ foldl (\acc x -> if x `elem` ones then acc ++ ["1"] else acc ++  ["_"] ) [] [1..100]

allLines :: [[Int]] -> [String]
allLines = (init . map line)  -- init because first amount of straights is -1 for the start tree

completeGrid :: [String] -> [String]
completeGrid usedLines = (replicate (63- (length usedLines)) ( concat $ replicate 100 "_") ) ++ usedLines

main = do
  input <- getLine
  let n = read input :: Int
      grid = completeGrid $ allLines $ reverse $ levelValues $ foldr (\x acc -> iteration x acc) (singleton 50) [n,(n-1)..1]
  mapM_ putStrLn grid
