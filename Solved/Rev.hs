rev :: [Int] -> [Int]
rev [] = []
rev (x:xs) = rest ++ [x] where
  rest = rev xs
