reduce :: String -> String
reduce s = foldl (\acc x -> if x `elem` acc then acc else acc ++ [x] ) [] s

main :: IO ()
main = do
    a <- getLine
    putStrLn $ reduce a
