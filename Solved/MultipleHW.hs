
main :: IO ()
main = do
    n_temp <- getLine
    let n = read n_temp :: Int
    mapM_ putStrLn $ replicate n "Hello World"
    --  Print "Hello World" on a new line 'n' times.
