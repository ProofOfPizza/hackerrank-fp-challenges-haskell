-- https://www.hackerrank.com/challenges/fractal-trees/problem

  import Data.List
  main = do
    input <- getLine
    let n = read input :: Int
    mapM_  putStrLn $ map line $ reverse $ ones 5 n 63 100

  line :: [Int] -> String
  line ones = concat $ foldl (\acc x -> if x `elem` ones then ac c ++ ["1"] else acc ++  ["_"] ) [] [1..100]

  ones :: Int -> Int -> Int -> Int -> [[Int]]
  ones maxIterations iterations maxRows maxColumns
    | any (<= 0)[maxIterations, iterations, maxRows, maxColumns] = [[]]
    | otherwise = foldl (\acc iteration -> acc ++ vertical maxColumns maxIterations iteration ++ slanting maxIterations iteration) [] [1..iterations] ++ replicate (maxRows - (sum $ map (\x -> (2^(6-x))) [1..iterations])) []

  vertical :: Int -> Int -> Int -> [[Int]]
  vertical maxColumns maxIterations iteration =
    replicate (2^(maxIterations-iteration)) (onesAtIteration (iteration - 1))

  slanting :: Int -> Int -> [[Int]]
  slanting maxIterations iteration = init $ foldl (\acc x -> [widenOnes (onesAtIteration iteration) x] ++ acc ) [splitOnes $ onesAtIteration iteration] [0..(2^(maxIterations-iteration)-1)] where
    splitOnes ones = concat $ map (\x -> [x-1, x+1]) ones
    widenOnes ones x = zipWith (+)  ones $ concat $  repeat [(-x),x]

  onesAtIteration :: Int -> [Int]
  onesAtIteration iteration = map (+50) $ foldl (\ax ay -> concat $ map (\bx -> [bx+2^(5-ay), bx-2^(5-ay)]) ax) [0] [1..iteration]
