-- https://www.hackerrank.com/challenges/convex-hull-fp/problem
-- graham scan https://www.geeksforgeeks.org/convex-hull-set-2-graham-scan/

import Text.Printf
import Data.List
import Data.Monoid
import Control.Monad.State

type Point = (Double, Double)
type Stack = [Point]


switch :: Point -> Point
switch (a,b) = (b,a)

orientation :: Point -> Point -> Double
orientation (x1, y1) (x2,y2) = (y2-y1) / (x2-x1)

pOrient start p q = compare (orientation start q) (orientation start p) `mappend` compare (distance start p) (distance start q)

distance :: Point -> Point -> Double
distance (x1,y1) (x2,y2) = sqrt $ (x1-x2)^2 + (y1-y2)^2

shift :: [Point] -> [Point]
shift xs = [xs] >>= (\x -> tail x ++ [head x])

excludeInternalPoints :: [Point] -> [Point]
excludeInternalPoints [] = []
excludeInternalPoints xs =
  reverse $ foldl (\acc n -> recurseAndAccept n acc) [] xs

recurseAndAccept:: Point -> [Point] -> [Point]
recurseAndAccept n acc
  | acc == [] = [n]
  | length (acc) < 3 = n:acc -- ?? of 2
  | otherwise = if checkClockwise [ head $ tail acc, head acc, n]
    then n:acc
    else recurseAndAccept n $ tail acc

checkClockwise :: [Point] -> Bool
checkClockwise [(x1, y1), (x2,y2),(x3,y3)] = (y2 - y1)*(x3 - x2) - (y3 - y2)*(x2 - x1) < 0

ordered :: [Point] -> [Point]
ordered ps = (order $ filter (\(x,y) -> x > (fst start) ) ps') ++ (filter (\(x,y) -> x==(fst start) ) ps') ++ (reverse $ order $ filter (\(x,y) -> x<(fst start) ) ps') where
  start = head $ map switch $ sortBy compare $ map switch ps
  ps' = filter (/= start) ps
  order xs = sortBy (\p q -> compare (abs (orientation start p)) (abs (orientation start q))) xs

findHull :: [Point] -> [Point]
findHull points = excludeInternalPoints $ filtered $ ordered points where
  start = head $ map switch $ sortBy compare $ map switch points
  filtered ps' = [start] ++ (foldr (\p acc -> if ((orientation start $ head acc) /= (orientation start p)) then p : acc else if ((distance start $ head acc) > (distance start p)) then acc else p : tail acc) [last ps'] ps')
  doubleShifted ps'' =  map (\(a,(b,c)) -> [a,b,c]) $ zip ps'' $ zip (shift ps'') (shift $ shift ps'')

perimeterOfPolygon :: [Point] -> Double
perimeterOfPolygon pts
  | pts == [] = 0.0
  | otherwise = sum $ zipWith (\p1 p2 -> distance p1 p2) pts (shift pts)

solve :: [(Int, Int)] -> Double
solve ps = perimeterOfPolygon $ findHull points where
  points = map (\(x,y) -> (fromIntegral x :: Double, fromIntegral y :: Double))  ps


main :: IO ()
main = do
n <- readLn :: IO Int
content <- getContents
let
  points = map (\[x, y] -> (x, y)). map (map (read::String->Int)). map words. lines $ content
  ans = solve points
printf "%.1f\n" ans
