## Hackerrank FP challenges in Haskell

Here I have put some of the solutions for challenges I solved using Haskell. I am learning haskell, and the [HackerRank FP challenges](https://www.hackerrank.com/domains/fp/intro) seemed like a fun thing to do.

Please note that none of these solutions pretend to be the best or anything close. They are just the solutions I came up with. If you have any ideas that may help a fellow coder on his way to amazingness feel please feel free to suggest them!

I hope you may feel inspired to also try out some of these challenges, or maybe just find something here that will help you along your way as well.

Stay excellent!

The challenges I solved are:

1. [AbsList](./Solved/AbsList.hs)
2. [AreaOfPolygon](./Solved/AreaOfPolygon.hs)
3. [EuclidianAlgorithm](./Solved/EuclidianAlgorithm.hs)
4. [Fibonacci](./Solved/Fibonacci.hs)
5. [FilterPositions](./Solved/FilterPositions.hs)
6. [Integrals](./Solved/Integrals.hs)
7. [Intro](./Solved/Intro.hs ) -- really? uhm..
8. [IsFunction](./Solved/IsFunction.hs)
9. [LengthList](./Solved/LengthList.hs)
10. [ListReplication](./Solved/ListReplication.hs)
11. [MultipleHelloWorlds](./Solved/MultipleHW.hs)
12. [PerimeterOfConvexHull](./Solved/PerimeterOfConvexHull.hs)
13. [PrintTreeRecursion](./Solved/PrintTreeRecursion.hs) -- solution using only lists
14. [PrintTreeRecursionWithTriTree](./Solved/PrintTreeRecursionWithTriTree.hs) -- using ternary trees
15. [Reverse](./Solved/Rev.hs)
16. [StringCompression](./Solved/StringCompression.hs)
17. [StringMingling](./Solved/StringMingling.hs)
18. [StringOPermute](./Solved/StringOPermute.hs)
19. [StringPrefixReduction](./Solved/StringPrefixReduction.hs)
20. [SumOfOddElements](./Solved/SumOfOddElements.hs)
